# Test IoT Hub

This repo contains Terraform code to spin up an IoT Hub for testing purposes and a Python script to test sending messages to the Hub.

## Authenticating using the Azure CLI

[Azure Provider: Authenticating using the Azure CLI](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/guides/azure_cli)

```{bash}
az login
az account list
```

Select the right subscription:

```{bash}
az account set --subscription="SUBSCRIPTION_ID"
```


## Create an IoT Hub with terraform

```{bash}
cd terraform
terraform init
terraform plan
terraform apply
```

## Register a device in the IoT Hub

Follow the [documentation to register a device](https://learn.microsoft.com/en-us/azure/iot-develop/quickstart-send-telemetry-iot-hub#register-a-device).

Copy the device's primary connection string.

## Create Python virtual env and install packages

Create venv and install packages:

```{bash}
cd scripts
python -m venv env
source env/bin/activate
pip install -r requirements.txt
```

## Send a test message

You can monitor events in the hub with this command:

```{bash}
az iot hub monitor-events --hub-name <hub name> --output json
```

This does not seem to work in combination with routes and endpoints.

Export the primary connection string for the device and run the Python script:

```{bash}
export IOTHUB_DEVICE_CONNECTION_STRING="<connection string>"
python send_message.py
```

You can find the message in the storage container created for the IoT Hub.
Note: because messages are send in batches it may take a couple minutes before the file is created in the storage container.

## Clean up

```{bash}
terraform destroy
```

## Resources

[Azure IoT Hub Documentation](https://learn.microsoft.com/en-us/azure/iot-hub/)

[Terraform provider for Azure IoT Hub](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/iothub)

[Connecting IoT Devices to Azure: IoT Hub and Event Hubs](https://learn.microsoft.com/en-us/azure/iot-hub/iot-hub-compare-event-hubs)
